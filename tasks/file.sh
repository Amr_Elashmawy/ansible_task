#!/bin/bash

ansible all -m user -a "name='ansible' state='present' password='{{ 'password' | password_hash('sha512') }}'" -u root
ansible all -m authorized_key -a "user='ansible' state='present' key='{{ lookup('file','/home/ansible/.ssh/id_rsa.pub')}}'" -u root
ansible all -m lineinfile -a "path=/etc/sudoers state=present line='ansible ALL=(ALL) NOPASSWD: ALL' backup=yes validate='/usr/sbin/visudo -cf %s'" -u root
